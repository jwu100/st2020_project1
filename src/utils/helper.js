
let sorting = (array) => {
    array.sort();
    return array;
}

let compare = (a, b) => {
    return a <= b;
    //return -1;
}

let average = (nums) => {
    //return 0;
    var sum = 0;
    for( var i = 0; i < nums.length; i++ ){
        sum += nums[i];
    }

    var avg = sum/nums.length;

    if (avg % 1 == 0) {
        return avg;
    }

    avg = avg.toFixed(2);
    return parseFloat(avg);
}


module.exports = {
    sorting,
    compare,
    average
}
